import { Quote } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { GameQuestion } from '../gamequestion';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: GameQuestion[] = [];
  private activeQuestion: GameQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  private points: number = 0;
  private correctAnswers: number = 0;
  private endPoints: number;
  private correctness: number;
  private endQuote: string;
 

  constructor() { }

  public addQuestion() {
    this.questions = [{
      quote: 'Who manufactured the first commercially succesful Pong-videogame?',
      options: [
        'Namco',
        'Atari',
        'Konami'],
      correctOption: 1
    },

    {
      quote: 'Which is the correst Konami Code sequence?',
      options: [
        'Up Up Down Down Left Right Left Right B A',
        'Up Down Up Down',
        'Left Left Right Right Up A B',
        'None of the above'],
      correctOption: 0
    },

    {
      quote: 'In what year was the Super Mario Bros. released on NES-console?',
      options: [
        '1988',
        '1990',
        '1985',
        '1983'],
      correctOption: 2
    },

    {
      quote: 'Which video game title was released first?',
      options: [
        'Super Mario Bros',
        'Mortal Kombat',
        'Bubble Bobble',
        'Donkey Kong'],
      correctOption: 3
    }
  ];
  }

  public setQuestion() {
    this.optionCounter = 0;
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }

  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
    this.points = 0;
    this.correctAnswers = 0;
    this.endPoints = 0;
    this.correctness = 0;
    this.endQuote = "";
  }

  public getQuestions(): GameQuestion[] {
    return this.questions;
  }

  public getActiveQuestion(): GameQuestion {
    return this.activeQuestion;
  }

  public getQuoteOfActiveQuestion(): string {
    return this.activeQuestion.quote;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter(): number {
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++
    return (this.optionCounter > this.activeQuestion.options.length) ? true : false;
  }

  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }

  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

  public getNumberofCorrectAnswers(): number {
    return this.correctAnswers;
  }

  public getNumberofPoints(): number {
    return this.points;
  }

  public addPoints() {
    this.points++;
    this.correctAnswers++;
  }

}

