import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';
import { QuestionPage } from '../question/question.page';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})

export class AnswersPage implements OnInit {
  


  constructor(public router: Router,
    public activatedRoute: ActivatedRoute,
    private quizService: QuizService) { }


  ngOnInit() {
    this.quizService.initialize();
    this.quizService.addQuestion();
  } 

  private checkOption(option: number) {
    this.quizService.setOptionSelected();
    if (this.quizService.areAllOptionsUsed() ) {
      this.quizService.setQuestion();
    }
    else {
      if (this.quizService.isCorrectOption(option)) {
        this.quizService.getCorrectOptionOfActiveQuestion();
      }
    }
  }
  

  private goHome() {
    this.quizService.initialize();
    this.router.navigateByUrl('home');
}

}
