import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})

export class QuestionPage {


    private feedback: string;
    private duration: number;

  constructor(public router: Router,
    public activatedRoute: ActivatedRoute,
     private quizService: QuizService) {}

  ngOnInit() {
    this.quizService.addQuestion();
    this.quizService.initialize();
    this.feedback = "";
  }

  private checkOption(option: number) {
    this.quizService.setOptionSelected();
      
      if (this.quizService.isCorrectOption(option)) {
        this.quizService.addPoints();
        this.feedback = 
        this.quizService.getCorrectOptionOfActiveQuestion() + 
        ' is correct! Go on...';
      }
      else {
        if (this.quizService.isFinished()) {
          this.duration = this.quizService.getDuration();
          this.router.navigateByUrl('result/' + this.duration);
        }
        else {
          this.feedback = 'Incorrect. ' + 
          this.quizService.setQuestion();
          (this.quizService.getNumberOfOptionsOfActiveQuestion() - 
          this.quizService.getIndexOfOptionCounter());
        }
      }
  }

  private continue() {
    if (this.quizService.isFinished()) {
      this.duration = this.quizService.getDuration();
      this.router.navigateByUrl('result/' + this.duration);
    }
    else {
      this.quizService.setQuestion();
    }
  }

}

