import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from '../quiz.service';
import { QuestionPage } from '../question/question.page';
import { AnswersPage } from '../answers/answers.page';



@Component({
    selector: 'app-result',
    templateUrl: './result.page.html',
    styleUrls: ['./result.page.scss'],
})


export class ResultPage {

    private duration: number;
    private durationSeconds: number;
    private endPoints: number;
    private correctness: number;
    private endQuote: string;
    private endPicture: string;
    private percentage: number;

    constructor(public router: Router,
        public activatedRoute: ActivatedRoute,
        private quizService: QuizService) { }

    ngOnInit() {
        this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
        this.durationSeconds = Math.round((this.duration) / 1000);
        this.endPoints = this.quizService.getNumberofPoints();       
        this.getEndResult();
          }


    private goHome() {
        this.quizService.initialize();
        this.router.navigateByUrl('home');
    }

    private goToAnswersPage() {
        this.quizService.initialize();
        this.router.navigateByUrl('/answers');
      }


    public getEndResult() {

      this.correctness = this.quizService.getNumberofCorrectAnswers();
      this.percentage = this.endPoints / 4 * 100;

        if (this.correctness === 4 && this.durationSeconds < 15 ) {
          this.endQuote = 'You\'re breathtaking!';
          document.getElementById("best").hidden = false;
          document.getElementById("middle").hidden = true;
          document.getElementById("loser").hidden = true;
        }
        else if ((this.correctness > 2 && this.durationSeconds >= 15) || (this.correctness === 3 && this.durationSeconds <15)) {
            this.endQuote = 'Praise the Sun! You did well!'
            document.getElementById("best").hidden = true;
            document.getElementById("middle").hidden = false;
            document.getElementById("loser").hidden = true;
          }
        else if (this.correctness <= 2) { 
          this.endQuote = 'Umm, that didn\'t go well. Maybe try again?'
          document.getElementById("best").hidden = true;
          document.getElementById("middle").hidden = true;
          document.getElementById("loser").hidden = false;
        }
        else { 
          this.endQuote = 'Oh god, something went wrong. Maybe try again?'
          document.getElementById("best").hidden = true;
          document.getElementById("middle").hidden = true;
          document.getElementById("loser").hidden = false;
        }
      }


}
